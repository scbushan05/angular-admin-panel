import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public flag: boolean = false;
  
  public showMore: boolean = false;
  public showMoreGoods: boolean = false;
  public showMoreEmployees: boolean = false;

  showMoreItems = (text) => {
    if (text === 'users') {
      this.showMore = !this.showMore;
    }else if (text === 'goods'){
      this.showMoreGoods = !this.showMoreGoods;
    } else if (text === 'employees') {
      this.showMoreEmployees = !this.showMoreEmployees;
    }
  }

  toggleSidebar = () => {
    this.flag = !this.flag;
  }
}
